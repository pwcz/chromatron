#!/bin/bash
input_file="input.txt"
chromatron_output="chromatron_output.txt"
map='10 10 
10
ES 8 4 6 110
TG 2 4 0 110
BL 3 5 0 0'
a=$(($(echo "$map" | wc -l)-2))
b=$(echo $map | cut -d' ' -f 3)
if (( "$a" > "$b" ))
then
	echo "Wczytałem tylko $b pierwszych elementow"
elif (( "$a" < "$b" ))
then
	diff=$((b - a))
	echo "Jest o $diff za malo elementow (dopisze $diff luster)"
	for (( i=0; $i < $diff; i++ )) ; do
       map=$map"
LU 0 0 0 0"
	done
fi
echo "$map" > $input_file

#g++ chromatron.cpp -std=c++11 #Kompilacja
make
./chromatron.exe < $input_file > $chromatron_output
python judge.py $input_file $chromatron_output

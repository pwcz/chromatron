#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include <map>
#include <random>
#include <fstream>
#include <new> 
#include <cassert>

#define DEBUG false

using namespace std;
using uint = unsigned int;

// =============================================== CELL TYPE ===========================================================

enum cell_type { ES = 0, LU = 1, TG = 2, BL = 3, NONE = 4 };

inline cell_type toCellType(string s);
inline string cellTypeToString(cell_type dt);
inline bool isLaser(cell_type dt);
inline bool isMirror(cell_type dt);
inline bool isBlock(cell_type dt);
inline bool isTarget(cell_type dt);

inline cell_type toCellType(string s) {
	// Laser
	if (s == "ES") return ES;
	// Mirror
	if (s == "LU") return LU;
	// Target    
	if (s == "TG") return TG;
	// Block
	if (s == "BL") return BL;
	// None - default type for rays or empty cells
	if (s == "NONE") return NONE;
	return NONE;
}

inline string cellTypeToString(cell_type dt) {
	switch (dt) {
		case ES: return "ES";
		case LU: return "LU";
		case TG: return "TG";
		case BL: return "BL";
		case NONE: return "NONE";
	}
	return "NONE";
}

inline bool isLaser(cell_type dt) {
	return dt == ES;
}

inline bool isMirror(cell_type dt) {
	return dt == LU;
}

inline bool isBlock(cell_type dt) {
	return dt == BL;
}

inline bool isTarget(cell_type dt) {
	return dt == TG;
}

// =============================================== COLOR TYPE ==========================================================

enum color_type { BLANK = 0, BLUE = 1, GREEN = 2, CYAN = 3, RED = 4, MAGENTA = 5, YELLOW = 6, WHITE = 7 };

inline color_type toColor(string s);
inline string colorToString(color_type c);

inline color_type toColor(string s) {
	if (s == "000") return BLANK;
	if (s == "001") return BLUE;
	if (s == "010") return GREEN;
	if (s == "011") return CYAN;
	if (s == "100") return RED;
	if (s == "101") return MAGENTA;
	if (s == "110") return YELLOW;
	if (s == "111") return WHITE;
	return BLANK;
}

inline string colorToString(color_type c) {
	switch (c) {
		case BLANK: return "000";
		case BLUE: return "001";
		case GREEN: return "010";
		case CYAN: return "011";
		case RED: return "100";
		case MAGENTA: return "101";
		case YELLOW: return "110";
		case WHITE: return "111";
	}
	return "000";
}

inline color_type operator+=(color_type& c1, color_type c2) {
	if ((color_type)c1 == BLANK) return c1 = c2;
	if (c2 == BLANK) return c1;
	if ((color_type)c1 == RED && c2 == GREEN) return c1 = YELLOW;
	if ((color_type)c1 == GREEN && c2 == BLUE) return c1 = CYAN;
	if ((color_type)c1 == RED && c2 == BLUE) return c1 = MAGENTA;
	if (c2 == RED && (color_type)c1 == GREEN) return c1 = YELLOW;
	if (c2 == GREEN && (color_type)c1 == BLUE) return c1 = CYAN;
	if (c2 == RED && (color_type)c1 == BLUE) return c1 = MAGENTA;
	return c1;
}

// ================================================ RAY TYPE ===========================================================

struct ray_type {
	unsigned short direction;
	color_type color;

	bool operator==(ray_type r) {
		return (direction == r.direction && color == r.color);
	}
};

// ================================================== CELL =============================================================

class Cell {
	cell_type type;
	uint x;
	uint y;
	color_type color;
	/*
	* directions (from laser definition):
	* _________
	* |7  6  5|
	* |0  x  4|
	* |1  2  3|
	* ---------
	*/
	unsigned short direction;
	vector<ray_type> rays;
public:
	Cell();
	Cell(cell_type type, uint x, uint y, unsigned short direction, color_type color);
	Cell(string type, uint x, uint y, unsigned short direction, string color);
	cell_type getCellType() const;
	uint getX() const;
	void setX(uint x);
	uint getY() const;
	void setY(uint y);
	color_type getColor() const;
	unsigned short getDirection()const;
	void setDirection(unsigned short direction);
	friend ostream& operator<<(ostream& os, const Cell& c);
	friend bool operator==(Cell c1, Cell c2);
	void addRay(ray_type ray);
	vector<ray_type>& getRays();
	const vector<ray_type>& getRays() const;
	void clearRays();
};

// może się okazać potem że lepszy będzie np. float
using objective_t = uint;

struct Board {
	Board() {}
	Board(uint width, uint height) : cells(width, vector<Cell>(height)) {}
	vector<vector<Cell>> cells;
	vector<Cell> lasers;
};

using BoardPtr = shared_ptr<Board>;

struct Solution {
	Board board;
	objective_t objectiveValue = 0;
};


Cell::Cell() : direction(0) {
	this->x = 0;
	this->y = 0;
	this->type = NONE;
	this->color = BLANK;
}

Cell::Cell(cell_type type, uint x, uint y, unsigned short direction, color_type color) {
	this->type = type;
	this->x = x;
	this->y = y;
	this->direction = direction;
	this->color = color;
}

Cell::Cell(string type, uint x, uint y, unsigned short direction, string color) {
	this->type = toCellType(type);
	this->x = x;
	this->y = y;
	this->direction = direction;
	this->color = toColor(color);
}

cell_type Cell::getCellType() const {
	return type;
}

uint Cell::getX() const {
	return x;
}

void Cell::setX(uint x) {
	this->x = x;
}

uint Cell::getY() const {
	return y;
}

void Cell::setY(uint y) {
	this->y = y;
}

color_type Cell::getColor() const {
	return color;
}

unsigned short Cell::getDirection()const {
	return direction;
}

void Cell::setDirection(unsigned short direction) {
	this->direction = direction;
}

ostream& operator<<(ostream& os, const Cell& c) {
	if (isBlock(c.type)) {
		os << "B";
		return os;
	}
	if (isLaser(c.type)) {
		os << "*";
		return os;
	}
	if (isMirror(c.type)) {
		if (c.rays.empty()) {
			switch (c.type) {
				case LU:
					os << "L";
					break;
				default:
					os << ".";
			}
			return os;
		}
		os << c.direction;
		return os;
	}
	if (isTarget(c.type)) {
		os << "o";
		return os;
	}
	if (!c.rays.empty()) {
		switch (c.rays[0].direction) {
			case 0:
			case 4:
				os << "-";
				break;
			case 1:
			case 5:
				os << "/";
				break;
			case 2:
			case 6:
				os << "|";
				break;
			case 3:
			case 7:
				os << "\\";
				break;
			default:
				os << ".";
		}
		return os;
	} else if (c.type == NONE) {
		os << ".";
		return os;
	}
	return os;
}

bool operator==(Cell c1, Cell c2) {
	return (c1.type == c2.type && c1.x == c2.x && c1.y == c2.y && c1.direction == c2.direction && c1.color == c2.color);
}

void Cell::addRay(ray_type ray) {
	rays.push_back(ray);
}

vector<ray_type>& Cell::getRays() {
	return rays;
}

const vector<ray_type>& Cell::getRays() const {
	return rays;
}

void Cell::clearRays() {
	rays.clear();
}

// ============================================== DECLARATIONS =========================================================

void emitRay(Board &board, uint width, uint height, Cell& laser, bool clear);
void clearRays(Board& board);
pair<short, short> getRaySteps(unsigned short direction);
bool solve(Board &board, uint width, uint height,
	vector<Cell> allMirrors, vector<Cell>& mirrorsToUse,
	map<long, bool> &mirrorsSnapshots, vector<Cell>& lasers, bool reverse, bool right_up, bool left_down);
void printBoard(Board board, uint width, uint height);
void putMirror(Board &board, uint width, uint height, uint x, uint y, Cell &mirror, unsigned short mirrorDirection);
unsigned short getReflectionDirection(cell_type mirror_type, unsigned short mirrorDirection, unsigned short rayDirection);
bool isBoardCompleted(const Board& board, uint width, uint height);
void reflectRays(Board &board, uint width, uint height, Cell &mirror, bool clear);

Cell* getCellAt(uint x, uint y, Board& board);
const Cell* getCellAt(uint x, uint y, const Board& board);
objective_t evaluate(const Board& board, uint width, uint height);
void barriers(uint width, uint height, uint x, uint y, pair<uint, uint>& x_radius, pair<uint, uint>& y_radius);
void mutate(Board& board);
vector<Board> twoPointCrossover(const Board& parentA, const Board& parentB, uint width, uint height,const Board& templateBoard);
void setRandomMirrorsForBoard(Board& board, uint mirrors,Board& templateBoard);
vector<Solution> createInitialPopulation(const Board& referenceBoard, uint size);
vector<Solution> reproduce(const vector<Solution>& population, float crossoverProbability, float mutationProbability,uint width, uint height,const Board& templateBoard);
void evaluateSolution(Solution& s);
void evaluatePopulation(vector<Solution>& population);
vector<Solution> evolutionarySolve(const Board& referenceBoard, uint iterations, uint populationSize, float crossoverProbability, float mutationProbability,uint width, uint height,const Board& templateBoard);

uint width, height, devicesCount = 0;

int main() {
	vector<Cell> devices;
	vector<Cell> mirrors;
	vector<Cell> lasers;

	std::ifstream A_input("../../input.txt");
	assert(A_input.is_open());

	A_input >> width >> height;	
	A_input >> devicesCount;
	
	//scanf("%d %d", &width, &height);
	//scanf("%d", &devicesCount);
	// extend boards
	width++;
	height++;

	Board board(width, height);
	Board templateBoard(width, height);
	Board board_reverse(width, height);
	Board board_right(width, height);
	Board board_left(width, height);

	//cout << "TEST1" << endl;
	//cout << "width= " << width << " height= " << height << endl;
	//cout << "TEST1_2" << endl;
	//cout << "devicesCount= " << devicesCount << endl;
	//return 0;
	//std::ifstream A_input;
	//A_input.open("A.txt");
	while (!A_input.eof()){
	//for (uint i = 0; i < devicesCount; i++) {
		char type[3];
		char color[4];
		uint x, y = 0;
		unsigned short direction = 0;

		A_input >> type >> x >> y >> direction >> color;
		//scanf("%2s %d %d %hd %3s", type, &x, &y, &direction, color);
		
		//cout << type << " " << x << " " << y << " " << direction << " " << color << endl;
		string t = type;
		string c = color;
		Cell cell(t, x, y, direction, c);
		
		if ( !isMirror(cell.getCellType()) ) {

			//cout << "lustro " << endl;
		}

		if (isLaser(cell.getCellType())) {
			board.lasers.push_back(cell);
		}

		devices.push_back(cell);
		board.cells[x][y] = cell;
	}
	//cout << "TEST2" << endl;
	// add lasers and rays, gather mirrors
	/*
	for (auto &dev : devices) {
		if (isLaser(dev.getCellType())) {
			lasers.push_back(dev);
			emitRay(board, width, height, dev, false);
			//emitRay(board_reverse, width, height, dev, false);
			//emitRay(board_right, width, height, dev, false);
			//emitRay(board_left, width, height, dev, false);

		}
		if (isMirror(dev.getCellType())) {
			mirrors.push_back(dev);
		}
	}*/
	//printBoard(board, width, height);
	//return 0;
	const auto POPULATION_SIZE = 200;
	const auto ITERATIONS = 200;
	const auto solutions = evolutionarySolve(board, ITERATIONS, POPULATION_SIZE, 0.9, 0.9, width,  height, templateBoard);
	const auto& bestSolution = solutions.front();
	std::cout << "Best solution score: " << bestSolution.objectiveValue << "\n";
	printBoard(solutions.front().board, width, height);
	// solve
	//map<long, bool> snapshots = map<long, bool>();
	// if (solve(board, width, height, mirrors, mirrors, snapshots, lasers, false, false, false)) {
	//     if (DEBUG) {
	//         cout << "Solution found!" << endl;
	//     }
	//     printBoard(board, width, height);
	//     cout << "evaluation: " << evaluate(board, width, height) << endl;
	// }
	// else if (DEBUG) {
	//     cout << "Solution not found" << endl;
	// }
	/*
	cout << "Calculation and solution for reversed board" << endl;
	if (solve(board_reverse, width, height, mirrors, mirrors, snapshots, lasers, true, false, false)) {
		if (DEBUG) {
			cout << "Solution found!" << endl;
		}
		printBoard(board_reverse, width, height);
		cout << "evaluation: " << evaluate(board_reverse, width, height,templateBoard) << endl;
	} else if (DEBUG) {
		cout << "Solution not found" << endl;
	}

	cout << "Calculation and solution for right_up" << endl;
	if (solve(board_right, width, height, mirrors, mirrors, snapshots, lasers, false, true, false)) {
		if (DEBUG) {
			cout << "Solution found!" << endl;
		}
		printBoard(board_right, width, height);
		cout << "evaluation: " << evaluate(board_right, width, height,templateBoard) << endl;

	} else if (DEBUG) {
		cout << "Solution not found" << endl;
	}

	cout << "Calculation and solution for left_down" << endl;
	if (solve(board_left, width, height, mirrors, mirrors, snapshots, lasers, false, false, true)) {
		if (DEBUG) {
			cout << "Solution found!" << endl;
		}
		printBoard(board_left, width, height);
		cout << "evaluation: " << evaluate(board_left, width, height,templateBoard) << endl;

	} else if (DEBUG) {
		cout << "Solution not found" << endl;
	}
	*/
	return 0;
};

void printBoard(Board board, uint width, uint height) {
	devicesCount = 0;
	if (DEBUG) {
		for (uint y = 1; y < height; y++) {
			for (uint x = 1; x < width; x++) {
				auto& cell = board.cells[x][y];
				// to tak ma być?
				/*if (cell) {
					cout << *cell;
				} else {
					cout << ".";
				}*/
			}
			cout << endl;
		}
		cout << endl;
		cout << endl;
		return;
	}
	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
			auto& cell = board.cells[x][y];
			//if (cell) {
				cell_type type = cell.getCellType();
				const char* const ZERO = "0";
				const char* const type_str = cellTypeToString(type).c_str();
				if (isMirror(type)) {
					devicesCount++;
					continue;
				}
				if (isLaser(type)) {
					devicesCount++;
					continue;
				}
				if (isTarget(type)) {
					devicesCount++;
					continue;
				}
				if (isBlock(type)) {
					devicesCount++;
					continue;
				}
			//
			//} else {
			//	continue;
			//}
		}
	}

	printf("%d %d\n", width - 1, height - 1);
	printf("%d\n", devicesCount);
	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
			auto& cell = board.cells[x][y];
			//if (!cell) continue;
			cell_type type = cell.getCellType();
			const char* const ZERO = "0";
			const std::string type_str = cellTypeToString(type).c_str();
			if (isMirror(type)) {
				if (cell.getRays().empty()) {
					printf("%s %d %d %hd %s\n", type_str.c_str(), 0, 0, 0, ZERO);
				} else {
					printf("%s %d %d %hd %s\n", type_str.c_str(), x, y, cell.getDirection(), ZERO);
				}
				continue;
			}
			if (isLaser(type)) {
				printf("%2s %d %d %hd ", type_str.c_str(), x, y, cell.getDirection());
				printf("%3s\n", colorToString(cell.getColor()).c_str());
				continue;
			}
			if (isTarget(type)) {
				printf("%2s %d %d %hd ", type_str.c_str(), x, y, 0);
				printf("%3s\n", colorToString(cell.getColor()).c_str());
				continue;
			}
			if (isBlock(type)) {
				printf("%s %d %d %hd %s\n", type_str.c_str(), x, y, 0, ZERO);
				continue;
			}
		}
	}
}

bool solve(Board &board, uint width, uint height,
	vector<Cell> allMirrors, vector<Cell> &mirrorsToUse,
	map<long, bool> &mirrorsSnapshots, vector<Cell>& lasers, bool reverse, bool right_up, bool left_down) {
	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
			auto& cell = board.cells[x][y];
			cell.clearRays();
		}
	}
	for (auto laser : lasers) {
		emitRay(board, width, height, laser, false);
	}
	if (isBoardCompleted(board, width, height)) {
		return true;
	}
	if (DEBUG) {
		printBoard(board, width, height);
	}
	if (mirrorsToUse.empty()) {
		return false;
	}
	int stepY = 1;
	int stepX = 1;
	uint startX = 1;
	uint startY = 1;
	if (reverse) {
		stepY = -1;
		stepX = -1;
		startX = width - 1;
		startY = height - 1;
	}
	if (right_up) {
		stepY = -1;
		stepX = 1;
		startX = 1;
		startY = height - 1;
	}
	if (left_down) {
		stepY = 1;
		stepX = -1;
		startX = width - 1;
		startY = 1;
	}

	for (uint y = startY; y > 0 && y < height; y += stepY) {
		for (uint x = startX; x > 0 && x < width; x += stepX) {
			const auto& cell = board.cells[x][y];
			if (cell.getCellType() == NONE && !cell.getRays().empty()) {
				for (auto &mirror : mirrorsToUse) {
					unsigned short mirrorDirections = 8;
					bool foundNewSnapshot = false;
					for (unsigned short mirrorDirection = 0; mirrorDirection < mirrorDirections; mirrorDirection++) {

						bool put = false;
						for (auto ray : cell.getRays()) {
							try {
								unsigned short reflectionDirection = getReflectionDirection(mirror.getCellType(), mirrorDirection, ray.direction);
								const pair<short, short> &steps = getRaySteps(reflectionDirection);
								uint newX = x + steps.first;
								uint newY = y + steps.second;
								if (reflectionDirection >= 0 && reflectionDirection <= 7 && newX > 0 && newX < width
									&& newY > 0 && newY < height) {
									put = true;
								}
							} catch (int) {
							}
						}
						if (!put) {
							continue;
						}

						long mirrorsHashcode = 17;
						for (auto& m : allMirrors) {
							unsigned short direction = mirrorDirection;
							uint mirrorX = x;
							uint mirrorY = y;
							if (&m != &mirror) {
								direction = m.getDirection();
								mirrorX = m.getX();
								mirrorY = m.getY();
							}
							if (mirrorX == 0 && mirrorY == 0) continue;
							mirrorsHashcode = mirrorsHashcode * 31 + m.getCellType();
							mirrorsHashcode = mirrorsHashcode * 31 + mirrorX;
							mirrorsHashcode = mirrorsHashcode * 31 + mirrorY;
							mirrorsHashcode = mirrorsHashcode * 31 + direction;
						}
						if (mirrorsSnapshots.find(mirrorsHashcode) != mirrorsSnapshots.end()) {
							continue;
						}
						mirrorsSnapshots[mirrorsHashcode] = true;
						foundNewSnapshot = true;

						putMirror(board, width, height, x, y, mirror, mirrorDirection);

						vector<Cell> mirrorsCopy(mirrorsToUse);
						const vector<Cell>::iterator &iterator =
							find_if(mirrorsCopy.begin(), mirrorsCopy.end(), [&](Cell const& p) {
							return p == mirror;
						});
						mirrorsCopy.erase(iterator);

						if (solve(board, width, height, allMirrors, mirrorsCopy, mirrorsSnapshots,
							lasers, reverse, right_up, left_down)) {
							return true;
						}
						board.cells[x][y] = cell;

						mirror.clearRays();
						mirror.setX(0);
						mirror.setY(0);
						mirror.setDirection(0);
					}
					if (!foundNewSnapshot) continue;
					reverse = !reverse;
					rotate(mirrorsToUse.begin(), mirrorsToUse.end() - 1, mirrorsToUse.end());
					if (solve(board, width, height, allMirrors, mirrorsToUse, mirrorsSnapshots,
						lasers, reverse, right_up, left_down)) {
						return true;
					}
				}
			}
		}
	}
	return false;
}

void putMirror(Board &board, uint width, uint height, uint x, uint y,
	Cell &mirror, unsigned short mirrorDirection) {

	mirror.setDirection(mirrorDirection);
	mirror.setX(x);
	mirror.setY(y);
	board.cells[x][y] = mirror;

}

void reflectRays(Board &board, uint width, uint height, Cell &mirror, bool clear) {

	cell_type mirrorType = mirror.getCellType();

	for (auto ray : mirror.getRays()) {

		unsigned short reflectionDirection;
		try {
			reflectionDirection = getReflectionDirection(mirrorType, mirror.getDirection(), ray.direction);
		} catch (int) {
			continue;
		}

		if (reflectionDirection >= 0 && reflectionDirection <= 7) {
			Cell tmpLaser(NONE, mirror.getX(), mirror.getY(), reflectionDirection, ray.color);
			emitRay(board, width, height, tmpLaser, clear);
		}

	}
}

void emitRay(Board &board, uint width, uint height, Cell &laser, bool clear) {

	pair<short, short> steps = getRaySteps(laser.getDirection());

	if (steps.first == 0 && steps.second == 0) {
		return;
	}

	ray_type ray = {laser.getDirection(), laser.getColor()};
	for (pair<uint, uint> i(laser.getX() + steps.first, laser.getY() + steps.second);
	i.first > 0 && i.first < width && i.second > 0 && i.second < height;
		i.first += steps.first, i.second += steps.second) {

		Cell &cell = board.cells[i.first][i.second];
		cell_type cellType = cell.getCellType();
		if (isBlock(cellType) || isLaser(cellType)) {
			return;
		}
		cell.addRay(ray);
		if (isMirror(cellType)) {
			reflectRays(board, width, height, cell, clear);
			return;
		}
	}
}

void clearRays(Board& board) {
	for (auto& row : board.cells) {
		for (auto& cell : row) {
			cell.clearRays();
		}
	}
}

unsigned short getReflectionDirection(cell_type mirror_type, unsigned short mirrorDirection, unsigned short rayDirection) {
	switch (mirror_type) {
		case LU:
			switch (mirrorDirection) {
				case 0:
					if (rayDirection == 3) return 1;
					if (rayDirection == 5) return 7;
					throw 3;
				case 1:
					if (rayDirection == 4) return 2;
					if (rayDirection == 6) return 0;
					throw 3;
				case 2:
					if (rayDirection == 5) return 3;
					if (rayDirection == 7) return 1;
					throw 3;
				case 3:
					if (rayDirection == 0) return 2;
					if (rayDirection == 6) return 4;
					throw 3;
				case 4:
					if (rayDirection == 1) return 3;
					if (rayDirection == 7) return 5;
					throw 3;
				case 5:
					if (rayDirection == 0) return 6;
					if (rayDirection == 2) return 4;
					throw 3;
				case 6:
					if (rayDirection == 1) return 7;
					if (rayDirection == 3) return 5;
					throw 3;
				case 7:
					if (rayDirection == 2) return 0;
					if (rayDirection == 4) return 6;
					throw 3;
				default:
					throw 4;
			}
		default:
			throw 2;
	}
}

pair<short, short> getRaySteps(unsigned short direction) {
	switch (direction) {
		case 0:
			return make_pair(-1, 0);
		case 1:
			return make_pair(-1, 1);
		case 2:
			return make_pair(0, 1);
		case 3:
			return make_pair(1, 1);
		case 4:
			return make_pair(1, 0);
		case 5:
			return make_pair(1, -1);
		case 6:
			return make_pair(0, -1);
		case 7:
			return make_pair(-1, -1);
		default:
			return make_pair(0, 0);
	}
}

bool isBoardCompleted(const Board& board, uint width, uint height) {
	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
			const auto& cell = board.cells[x][y];
			if (isTarget(cell.getCellType())) {
				vector<ray_type> rays = cell.getRays();
				color_type colorSum = BLANK;
				for (auto ray : rays) {
					colorSum += ray.color;
				}
				if (colorSum != cell.getColor()) {
					return false;
				}
			}
		}
	}
	return true;
}

Cell* getCellAt(uint x, uint y, Board& board) {
	return (x < board.cells.size() && y < board.cells[0].size()) ? &board.cells[x][y] : nullptr;
}

const Cell* getCellAt(uint x, uint y, const Board& board) {
	return (x < board.cells.size() && y < board.cells[0].size()) ? &board.cells[x][y] : nullptr;
}

void mutate(Board& board) {
	// na poczatek sprobojmy randomowy obrot i przesuniecie o jeden
	// todo: parametryzacja prawdopodobienstw
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> disZeroOne(0, 1);
	const auto maxDirection = 7;
	uniform_int_distribution<> disDirectionChange(1, maxDirection);
	uniform_int_distribution<> disPositionChange(-1, 1);
	for (auto& row:  board.cells) {
		for (auto& cell : row) {
			if (isMirror(cell.getCellType())) {
				auto& mirror = cell;
				const auto mutate = disZeroOne(gen);
				if (mutate) {
					const auto rotate = disZeroOne(gen);
					if (rotate) {
						mirror.setDirection((mirror.getDirection() + disDirectionChange(gen)) % (maxDirection + 1));
					}
					const auto move = disZeroOne(gen);
					if (move) {
						const auto xChange = disPositionChange(gen);
						const auto yChange = disDirectionChange(gen);
						// tu sie pojawia pytanie czy jak zmieniamy pozycje to dopuszczamy niedozwolona
						// i sprawdzamy poprawnosc genotypu na koncu (lub nie sprawdzamy) - do ustalenia
						// narazie przesuwamy tylko jezeli sie da
						const auto newX = mirror.getX() + xChange;
						const auto newY = mirror.getY() + yChange;
						const auto c = getCellAt(newX, newY, board);
						if (c && c->getCellType() == NONE) {
							mirror.setX(newX);
							mirror.setY(newY);
							// na miejsce lustra dajemy pusta komorke 
							swap(mirror, *c);
						}
					}
				}
			}
		}
	}
}

vector<Board> twoPointCrossover(const Board& parentA, const Board& parentB, uint width, uint height,const Board& templateBoard) {
	bool debugCrossover = 0;
	if (debugCrossover){
		cout << "start " << endl;
	}
	Board children_A = templateBoard;
	Board children_B = templateBoard;
	vector<Cell> mirrors1,children1;
	vector<Cell> mirrors2,children2;
	
	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
				if (isMirror(parentA.cells[x][y].getCellType())) {
					mirrors1.push_back(parentA.cells[x][y]);
				}
				if (isMirror(parentB.cells[x][y].getCellType())) {
					mirrors2.push_back(parentB.cells[x][y]);
				}					
		}
	}
	uint max = 0;
	uint min = 1;
	if (debugCrossover){
		cout << "parent1= " << mirrors1.size() << " parent2= " << mirrors2.size() << endl;
	}
	
	if ( mirrors1.size() == 1 || mirrors2.size() == 1 ){
		max = 0;
		min = 0;
	}else{
		max = ((mirrors1.size()>mirrors2.size())?(mirrors2.size()):(mirrors1.size())) - 1; // max(mirrors1.size() - 1, mirrors2.size() - 1);
		//min = 0;
	}
	
	if ( min == max){
		min--;
	}
	//cout << 
	if (debugCrossover){
		cout << "max= " << max << " min= " << min << endl;
	}
	

	uint pivot1 = min + (rand() % (int)(max / 2 - min + 1));
	uint pivot2 = pivot1 + 1 + (rand() % (int)(max - pivot1));
	uint x,y;
	if (debugCrossover){
		cout << "pivot1= " << pivot1 << " pivot2= " << pivot2 << endl;
	}
	
	// pierwsza czesc
	for (uint i = 0; i < pivot1; i++) {
		children1.push_back(mirrors1[i]);
		children2.push_back(mirrors2[i]);
	}
	// pierwszy pivot druga czesc
	for (uint i = pivot1; i < pivot2; i++){
		children1.push_back(mirrors2[i]);
		children2.push_back(mirrors1[i]);
	}

	//trzecia czesc
	for (auto i = mirrors1.begin() + pivot2; i != mirrors1.end(); i++) {
		children1.push_back(*i);
	}
	for (auto i = mirrors2.begin() + pivot2; i != mirrors2.end(); i++) {
		children2.push_back(*i);
	}
	if (debugCrossover){
		cout << "children2= " << children2.size() << " children1= " << children1.size() << endl;
	}
	
	// wstawianie  luster do boarda
	for (auto i = children1.begin() ; i != children1.end(); i++) {
		children_A.cells[i->getX()][i->getY()] = *i;
	}
	for (auto i = children2.begin() ; i != children2.end(); i++) {
		children_B.cells[i->getX()][i->getY()] = *i;
	}	
	
	//return {};
	vector<Board> childrens;
	if (debugCrossover){
		cout << "wrzucanie " << endl;
	}
	childrens.push_back(children_A);
	childrens.push_back(children_B);
	if (debugCrossover){
		cout << "wrzucanie 2" << endl;
	}
	return childrens;
}

vector<Solution> selection(const vector<Solution>& population, uint numToSelect) {
	vector<Solution> selected(numToSelect);
	std::partial_sort_copy(population.begin(), population.end(),
		selected.begin(), selected.end(), [](const Solution& a, const Solution& b) {
		return a.objectiveValue > b.objectiveValue;
	});
	return selected;
}

// TODO
void setRandomMirrorsForBoard(Board& board, uint mirrors) {
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> distW(0, width);
	uniform_int_distribution<int> distH(0, height);
	uniform_int_distribution<int> direction(0, 7);
	uint complete=0;
	int x; 
	int y; 
	int d; 
	while(true){
		x = distW(gen);
		y = distH(gen);
		d = direction(gen);
		const auto c = getCellAt(x, y, board);
		if (c && c->getCellType() == NONE){
			Cell cell("LU", x, y, d, "0");
			board.cells[x][y] = cell;
			complete++;
		}
		if (complete == mirrors-1){
			break;
		}
	}
}

// tutaj trza wylosowac losowych ziomków
// board jest tutaj przekazywany żeby wiedzieć gdzie można lustra postawić
vector<Solution> createInitialPopulation(const Board& referenceBoard, uint size) {
	vector<Solution> initialPopulation(size);
	for (auto& solution : initialPopulation) {
		solution.board = referenceBoard;
		setRandomMirrorsForBoard(solution.board, 7);
	}
	return initialPopulation;
}

objective_t evaluate(Board& board, uint width, uint height) {
	objective_t evaluation = 0;
	uint mirrors_number = 0;
	
	for (auto laser : board.lasers) {
		emitRay(board, width, height, laser, false);
	}

	for (uint y = 1; y < height; y++) {
		for (uint x = 1; x < width; x++) {
			const auto cell = getCellAt(x, y, board);
			vector<ray_type> rays;

			if (!cell) continue;
			if (isTarget(cell->getCellType())) {
				pair<uint, uint> y_radius, x_radius;
				barriers(width, height, x, y, x_radius, y_radius);

				for (uint target_y = y_radius.first; target_y <= y_radius.second; target_y++) {
					for (uint target_x = x_radius.first; target_x <= x_radius.second; target_x++) {
						const auto cell_in = getCellAt(target_x, target_y, board);
						if (!cell_in) continue;
						rays = cell_in->getRays();
						if (rays.size() >= 1) {
							evaluation += 1;
							//cout << "Evaluation: " << evaluation << endl;
						}
					}
				}
			}

			if (isMirror(cell->getCellType())) {
				if (cell->getRays().empty()) {
					continue;
				} else { mirrors_number++; }
			}
		}
	}
	clearRays(board);
	
	//evaluation += mirrors_number;
	return evaluation;
}

void barriers(uint width, uint height, uint x, uint y, pair<uint, uint>& x_radius, pair<uint, uint>& y_radius) {
	uint radius = 2;
	if (x - radius <= 1) {
		x_radius.first = 1;
		x_radius.second = x + radius;
	} else if (x >= width - radius) {
		x_radius.first = x - radius;
		x_radius.second = width;
	} else {
		x_radius.first = x - radius;
		x_radius.second = x + radius;
	}

	if (y - radius <= 1) {
		y_radius.first = 1;
		y_radius.second = y + radius;
	} else if (y >= height - radius) {
		y_radius.first = y - radius;
		y_radius.second = height;
	} else {
		y_radius.first = y - radius;
		y_radius.second = y + radius;
	}
}

vector<Solution> reproduce(const vector<Solution>& population, float crossoverProbability, float mutationProbability,uint width, uint height,const Board& templateBoard) {
	random_device rd;
	mt19937 gen(rd());
	auto getRandomOther = [&](size_t index, const vector<Solution>& vec) -> const Solution& {
		uniform_int_distribution<size_t> dis(0, vec.size() - 1);
		auto randomOtherIndex = index;
		do {
			randomOtherIndex = dis(gen);
		} while (randomOtherIndex == index);
		return vec[randomOtherIndex];
	};

	vector<Solution> offsprings;
	uniform_real_distribution<float> dis(0, 1.f);
	for (auto i = 0u; i < population.size(); ++i) {
		const auto shouldReproduce = dis(gen) < crossoverProbability;
		if (shouldReproduce) {
			const auto& secondBoard = getRandomOther(i, population).board;
			vector<Board>  offspring = twoPointCrossover(population[i].board, secondBoard,width, height ,templateBoard);
			const auto shouldMutate = dis(gen) < mutationProbability;
			for (auto off : offspring) {
				if (shouldMutate) {
					mutate(off);
				}
				offsprings.emplace_back();
				offsprings.back().board = off;
				evaluateSolution(offsprings.back());
				// --> w tym miejscu opcjonalnie naprawa <--
			}
		}
	}
	return offsprings;
}

void evaluateSolution(Solution& s) {
	s.objectiveValue = evaluate(s.board, s.board.cells.size(), s.board.cells[0].size());
}

void evaluatePopulation(vector<Solution>& population) {
	for (auto& s : population) {
		evaluateSolution(s);
	}
}

vector<Solution> evolutionarySolve(const Board& referenceBoard, uint iterations, uint populationSize, float crossoverProbability, float mutationProbability,uint width, uint height,const Board& templateBoard) {
	auto population = createInitialPopulation(referenceBoard, populationSize);
	evaluatePopulation(population);
	// --> w tym miejscu opcjonalnie naprawa <--
	for (auto i = 0u; i < iterations; ++i) {
		std::cout << "Iteration " << i << "\n";
		auto offspring = reproduce(population, crossoverProbability, mutationProbability,width, height,templateBoard);
		population.insert(population.end(), offspring.begin(), offspring.end());
		evaluatePopulation(population);
		population = selection(population, populationSize);
	}
	return population;
}
